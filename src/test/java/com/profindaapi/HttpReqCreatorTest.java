package com.profindaapi;

import org.apache.http.client.methods.HttpPut;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.assertEquals;

public class HttpReqCreatorTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testCreateHttpRequestHasAccountDomainHeader() {

        environmentVariables.set("PROFINDAACCOUNTDOMAIN", "cfgtest.com");
        HttpPut testGet = new HttpPut("");
        HttpReqCreator reqCreator = new HttpReqCreator();

        reqCreator.createHttpRequest(testGet, "");

        assertEquals("PROFINDAACCOUNTDOMAIN", testGet.getHeaders("PROFINDAACCOUNTDOMAIN")[0].getName());
        assertEquals("profindaapitoken", testGet.getHeaders("profindaapitoken")[0].getName());
        assertEquals("application/vnd.profinda+json;version=2", testGet.getHeaders("Accept")[0].getValue());
        assertEquals("application/json; charset=UTF-8", testGet.getHeaders("Content-Type")[0].getValue());
    }
}