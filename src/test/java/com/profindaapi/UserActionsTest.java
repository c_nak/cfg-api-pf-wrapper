package com.profindaapi;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.profindaapi.Exceptions.UserNotFoundException;
import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class UserActionsTest {

    private UserActions actions = new UserActions();

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setEnvironmentVariables() {
        environmentVariables.set("PROFINDAACCOUNTDOMAIN", "cfgtest.com");
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(9001);

    @Test
    public void testUserSetPermission() throws Exception {
        int userId = 6000;
        int permissionId = 1010;
        String jsonResponse = TestJsonUpdateBodyResponse.testUpdateUserPermissionBodyResponse();

        WireMock.stubFor(put(urlEqualTo("/"+ userId))
                .willReturn(aResponse()
                        .withBody(jsonResponse)
                        .withStatus(200))) ;
        String endpoint = "http://localhost:9001/" + userId;

        HttpResponse updatedPermissionResponse = actions.updateUserPermissionById(endpoint, userId, permissionId, "" );

        verify(putRequestedFor(urlEqualTo("/"+userId))
                .withHeader("Content-Type", equalTo("application/json; charset=UTF-8"))
                .withRequestBody(equalToJson("{\"permissions_group_id\": " + permissionId  + "}")));

        assertEquals(200, updatedPermissionResponse.getStatusLine().getStatusCode());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUserDoesNotExist() throws Exception {
        String testEmail = "notExistingUser@nodomain.com";
        String jsonResponse = "{\n" +
                "  \"entries\": [],\n" +
                "  \"meta\": {\n" +
                "    \"total\": 0\n" +
                "  }\n" +
                "}";

        WireMock.stubFor(get(urlEqualTo("/notExistingUser@nodomain.com"))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withHeader("Content-Type", "\"application/json; charset=UTF-8\"")
                        .withHeader("Accept", "application/vnd.profinda+json;version=2")
                        .withBody(jsonResponse)));

        String endpoint = "http://localhost:9001/" + testEmail;

         actions.getUserIdByEmail(endpoint, testEmail, "");
    }

    @Test
    public void testRetrievesUserByEmail() throws Exception {
        String testEmail = "kisema@gmail.com";
        String jsonResponse = "{\n" +
                "  \"entries\": [\n" +
                "    {\n" +
                "      \"id\": 60907,\n" +
                "      \"first_name\": \"Cedric\",\n" +
                "      \"last_name\": \"Kisema\",\n" +
                "      \"email\": \"kisema@gmail.com\",\n" +
                "      \"created_at\": \"2018-02-15T12:15:47.563Z\",\n" +
                "      \"last_sign_in_at\": \"2018-04-01T19:25:46.047Z\",\n" +
                "      \"progress_completion\": 32,\n" +
                "      \"roles\": [\n" +
                "        \"admin\"\n" +
                "      ],\n" +
                "      \"status\": \"active\",\n" +
                "      \"hidden\": false\n" +
                "    }\n" +
                "  ],\n" +
                "  \"meta\": {\n" +
                "    \"total\": 2\n" +
                "  }\n" +
                "}";

        WireMock.stubFor(get(urlEqualTo("/kisema@gmail.com"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "\"application/json; charset=UTF-8\"")
                        .withHeader("Accept", "application/vnd.profinda+json;version=2")
                        .withBody(jsonResponse)));

        String endpoint = "http://localhost:9001/" + testEmail;

        long actionsUserIdByEmailresponse = actions.getUserIdByEmail(endpoint, testEmail, "");

        assertEquals(60907, actionsUserIdByEmailresponse);
    }

    @Test
    public void testUpdateUserThatExists() throws Exception {
        long testId = 6000;
        String jsonResponse = TestJsonUpdateBodyResponse.testUpdateUserCFGCert();

        WireMock.stubFor(put(urlEqualTo("/"+ testId))
                .willReturn(aResponse()
                        .withStatus(200)));

        String endpoint = "http://localhost:9001/" + testId;

        JSONParser parser = new JSONParser();

        JSONObject payload = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                "\"values\": [{\"id\": \"459390\",\"key\": \"beginners-front-end-web-development-html-css\",\"global_id\": null, \"curated_at\": null," +
                " \"admin_approved\": true, \"text\": \"Beginner's front end web development (HTML / CSS)\", \"experience\":null, \"interest\":null }]}]}");

        HttpResponse updateResponse = actions.updateUserById(endpoint, testId, "", payload);

        verify(putRequestedFor(urlEqualTo("/"+testId))
                .withHeader("Content-Type", equalTo("application/json; charset=UTF-8"))
                .withRequestBody(equalToJson(jsonResponse)));
        assertEquals(200, updateResponse.getStatusLine().getStatusCode());
    }

    @Test
    public void testUpdateUserThatDoesNotExists() throws Exception {
        int testId = 6000;
        String jsonResponse = TestJsonUpdateBodyResponse.testUpdateUserDoesNotExist();

        WireMock.stubFor(put(urlEqualTo("/"+ testId))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withBody(jsonResponse)));

        String endpoint = "http://localhost:9001/" + 6000;
        JSONParser parser = new JSONParser();
        JSONObject payload = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                "\"values\": [{\"id\": \"459390\",\"key\": \"beginners-front-end-web-development-html-css\",\"global_id\": null, \"curated_at\": null," +
                " \"admin_approved\": true, \"text\": \"Beginner's front end web development (HTML / CSS)\", \"experience\":null, \"interest\":null }]}]}");


        HttpResponse updateUserById = actions.updateUserById(endpoint, testId, "", payload);

        assertEquals("HTTP/1.1 404 Not Found", updateUserById.getStatusLine().toString());
    }
}
