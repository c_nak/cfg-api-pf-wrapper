package com.profindaapi;

import com.Utils;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class AuthenticationTest {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(9001);

    @Test
    public void testAuthenticateUser() throws Exception {

        envVars.set("PROFINDAACCOUNTDOMAIN", "test");

        String testEmail = "userExist@nodomain";
        String jsonResponse = "{\"authentication_token\":\"123456789\",\"id\":60907,\"email\":\"userExist@nodomain.com\"}";

        WireMock.stubFor(post(urlEqualTo("/authenticate"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "\"application/json; charset=UTF-8\"")
                        .withHeader("Accept", "application/vnd.profinda+json;version=2")
                        .withBody(jsonResponse)));

        String endpoint = "http://localhost:9001/authenticate";

        Authentication authentication = new Authentication(endpoint);

        HttpResponse authToken = authentication.authenticate(testEmail, "password");

        JSONObject authTokenResponse = Utils.getResponseAsJsonObject(authToken);

        assertEquals(jsonResponse, authTokenResponse.toJSONString());
    }
}
