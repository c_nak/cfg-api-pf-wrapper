package com.profindaapi;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TestJsonUpdateBodyResponse {
    public static String updateToCFGCert() throws Exception {
        return "{\n" +
                "    \"id\": 60907,\n" +
                "    \"first_name\": \"Cedric\",\n" +
                "    \"last_name\": \"Kisema\",\n" +
                "    \"gender\": \"female\",\n" +
                "    \"status\": \"active\",\n" +
                "    \"email\": \"kisema@gmail.com\",\n" +
                "    \"avatar\": {\n" +
                "        \"thumbnail_url\": \"https://profinda-saas-avatars.s3.amazonaws.com/uploads/production/profile/picture/60907/normal_profile_cedric-smile.jpg?X-Amz-Expires=604799&X-Amz-Date=20180404T202818Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJIPJRSNEMBXYOD5Q/20180404/us-east-1/s3/aws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=dff672de638a06802c4d8d07e4c9fd924c9029dd31396a42354b6a2a7e22155b\",\n" +
                "        \"full_url\": \"https://profinda-saas-avatars.s3.amazonaws.com/uploads/production/profile/picture/60907/huge_profile_cedric-smile.jpg?X-Amz-Expires=604799&X-Amz-Date=20180404T202818Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJIPJRSNEMBXYOD5Q/20180404/us-east-1/s3/aws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=76eb4eacf59fe4687cb847a18f86a5144f14ee7179ad71d7164a6ba6a392315c\",\n" +
                "        \"custom\": true\n" +
                "    },\n" +
                "    \"roles\": [\n" +
                "        \"admin\"\n" +
                "    ],\n" +
                "    \"registration_status\": \"active\",\n" +
                "    \"summary\": \"\",\n" +
                "    \"agree_to_terms_and_conditions\": null,\n" +
                "    \"contact_information\": [],\n" +
                "    \"last_activity_at\": \"2018-04-04T20:03:43.723Z\",\n" +
                "    \"hidden\": false,\n" +
                "    \"statistics\": {\n" +
                "        \"visits_count\": 0,\n" +
                "        \"skills_count\": 6,\n" +
                "        \"invite_acceptance_percentage\": \"0.0\",\n" +
                "        \"progress_completion\": 32,\n" +
                "        \"skills_ranked_count\": 0,\n" +
                "        \"points_sum\": 30,\n" +
                "        \"percentage_rating\": 0,\n" +
                "        \"documents_count\": 0,\n" +
                "        \"pending_change_requests_count\": 0,\n" +
                "        \"messages_sent_count\": 0,\n" +
                "        \"offered_help_count\": 0\n" +
                "    },\n" +
                "    \"custom_fields\": [\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1510,\n" +
                "                \"name\": \"sectors\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 416422,\n" +
                "                    \"key\": \"telecommunications-internet-software-service\",\n" +
                "                    \"value\": \"Telecommunications, Internet software & service\",\n" +
                "                    \"global_id\": null,\n" +
                "                    \"curated_at\": null,\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Telecommunications, Internet software & service\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1509,\n" +
                "                \"name\": \"languages\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 413118,\n" +
                "                    \"key\": \"japanese\",\n" +
                "                    \"value\": \"Japanese\",\n" +
                "                    \"global_id\": null,\n" +
                "                    \"curated_at\": null,\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Japanese\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 413084,\n" +
                "                    \"key\": \"english\",\n" +
                "                    \"value\": \"English\",\n" +
                "                    \"global_id\": null,\n" +
                "                    \"curated_at\": null,\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"English\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1518,\n" +
                "                \"name\": \"cfg_certifications\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 459390,\n" +
                "                    \"key\": \"beginners-front-end-web-development-html-css\",\n" +
                "                    \"value\": \"Beginner's front end web development (HTML / CSS)\",\n" +
                "                    \"global_id\": null,\n" +
                "                    \"curated_at\": null,\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Beginner's front end web development (HTML / CSS)\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1516,\n" +
                "                \"name\": \"job_title\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 416427,\n" +
                "                    \"key\": \"fullstack-software-developer-contract\",\n" +
                "                    \"value\": \"Fullstack Software Developer (Contract)\",\n" +
                "                    \"global_id\": null,\n" +
                "                    \"curated_at\": null,\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Fullstack Software Developer (Contract)\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1514,\n" +
                "                \"name\": \"company\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 416428,\n" +
                "                    \"key\": \"self-employed\",\n" +
                "                    \"value\": \"Self Employed\",\n" +
                "                    \"global_id\": \"hal_22869\",\n" +
                "                    \"curated_at\": \"2018-02-27T16:03:09.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Self Employed\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": {\n" +
                "                \"id\": 1505,\n" +
                "                \"name\": \"skills\"\n" +
                "            },\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"id\": 416431,\n" +
                "                    \"key\": \"java\",\n" +
                "                    \"value\": \"Java\",\n" +
                "                    \"global_id\": \"esco_22587\",\n" +
                "                    \"curated_at\": \"2018-02-15T13:00:05.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Java\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 416430,\n" +
                "                    \"key\": \"javascript\",\n" +
                "                    \"value\": \"JavaScript\",\n" +
                "                    \"global_id\": \"hal_18803\",\n" +
                "                    \"curated_at\": \"2018-02-15T12:42:15.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"JavaScript\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 416429,\n" +
                "                    \"key\": \"software-development\",\n" +
                "                    \"value\": \"Software Development\",\n" +
                "                    \"global_id\": \"ot_439\",\n" +
                "                    \"curated_at\": \"2018-02-15T13:00:22.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Software Development\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 416404,\n" +
                "                    \"key\": \"css\",\n" +
                "                    \"value\": \"CSS\",\n" +
                "                    \"global_id\": \"esco_15418\",\n" +
                "                    \"curated_at\": \"2018-02-15T13:00:12.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"CSS\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 416403,\n" +
                "                    \"key\": \"html\",\n" +
                "                    \"value\": \"HTML\",\n" +
                "                    \"global_id\": \"hal_18794\",\n" +
                "                    \"curated_at\": \"2018-02-15T12:16:24.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"HTML\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 416402,\n" +
                "                    \"key\": \"ruby\",\n" +
                "                    \"value\": \"Ruby\",\n" +
                "                    \"global_id\": \"esco_14399\",\n" +
                "                    \"curated_at\": \"2018-02-15T13:00:09.000Z\",\n" +
                "                    \"admin_approved\": true,\n" +
                "                    \"text\": \"Ruby\",\n" +
                "                    \"experience\": null,\n" +
                "                    \"interest\": null\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ],\n" +
                "    \"supervisor\": null,\n" +
                "    \"permissions_group\": {\n" +
                "        \"id\": 136,\n" +
                "        \"name\": \"default\"\n" +
                "    }\n" +
                "}";
    }

    public static String testUpdateUserDoesNotExist() {
        return "{\n" +
                "    \"errors\": [\n" +
                "        {\n" +
                "            \"status\": \"404\",\n" +
                "            \"title\": \"Not Found\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
    }

    public static String testUpdateUserCFGCert() {
        JSONParser parser = new JSONParser();
        try {
            JSONObject json = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                    "\"values\": [{\"id\": \"459390\",\"key\": \"beginners-front-end-web-development-html-css\",\"global_id\": null, \"curated_at\": null," +
                    " \"admin_approved\": true, \"text\": \"Beginner's front end web development (HTML / CSS)\", \"experience\":null, \"interest\":null }]}]}");

            return json.toJSONString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String testUpdateUserPermissionBodyResponse() {
        try {
            JSONParser parser = new JSONParser();

            JSONObject json = (JSONObject) parser.parse("{\n\"id\": 60904,\n\"first_name\": \"Sarah\",\n\"last_name\": " +
                    " \"Morris\",\n    \"email\": \"sarah.morris@profinda.com\",\n    \"created_at\": \"2018-02-15T12:05:25.119Z\",\n " +
                    "   \"last_sign_in_at\": \"2018-06-12T14:37:25.968Z\",\n    \"progress_completion\": 0,\n    \"roles\": [\n " +
                    "       \"admin\"\n    ],\n    \"status\": \"active\",\n    \"hidden\": false,\n    \"contact_information\": [],\n " +
                    "   \"permissions_group_id\": 167,\n    \"busy_periods\": []\n}");

            return json.toJSONString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
