package com;

import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {

    public static JSONObject getResponseAsJsonObject( HttpResponse response) throws IOException {
        String line;
        StringBuilder result = new StringBuilder();

        JSONParser parser = new JSONParser();
        JSONObject jsonResponse = null;

        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        try {
            jsonResponse = (JSONObject) parser.parse(result.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonResponse;
    }
}
