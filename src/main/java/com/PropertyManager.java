package com;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyManager {

  public Properties loadCredentials() {
    Properties prop = new Properties();
    InputStream input = null;

    try {

      String filename = "credentials.properties";
      input = App.class.getClassLoader().getResourceAsStream(filename);
      if (input == null) {
        System.out.println("Sorry, unable to find " + filename);
        return prop;
      }
      //load a properties file from class path, inside static method
      prop.load(input);

    } catch (IOException ex) {
      ex.printStackTrace();
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return prop;
  }
}
