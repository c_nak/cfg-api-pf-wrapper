package com;

import com.profindaapi.Authentication;
import com.profindaapi.UserActions;
import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;

import java.io.IOException;

public class App {
    public static void main(String args[]) throws Exception {

        PropertyManager prop = new PropertyManager();
        String email = prop.loadCredentials().getProperty("emailAddress");
        String password = prop.loadCredentials().getProperty("password");

        Authentication authentication = new Authentication();
        HttpResponse authTokenResponse = authentication.authenticate(email, password);

        JSONObject jsonResponse = null;

        try {
            jsonResponse = Utils.getResponseAsJsonObject(authTokenResponse);

        } catch (IOException e) {
            e.printStackTrace();
        }

        UserActions userActions = new UserActions();
        String authToken = (String) (jsonResponse != null ? jsonResponse.get("authentication_token") : null);

        userActions.updateUserPermissionById(null,60904, 169, authToken);

//        HttpResponse response = userActions.updateUserById(60907, authToken);
//        System.out.println(response.getStatusLine());

//        Long user = userActions.getUserIdByEmail("xxxxxxxx@gmail.com", authToken);
//        if (user != null) {
//            System.out.println(user);
//        }
    }
}
