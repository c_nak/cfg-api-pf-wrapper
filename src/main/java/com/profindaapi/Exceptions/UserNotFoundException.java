package com.profindaapi.Exceptions;

public class UserNotFoundException extends Exception {
    // Parameterless Constructor
    public UserNotFoundException() {}

    // Constructor that accepts a message
    public UserNotFoundException(String message)
    {
        super(message);
    }
}
