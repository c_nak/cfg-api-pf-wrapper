package com.profindaapi;

import org.json.simple.JSONObject;

import java.util.List;

public class ProfindaUser {

  private final String first_name;
  private final String last_name;
  private final String email;
  private List custom_fields;

  public ProfindaUser(String first_name, String last_name, String email) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public JSONObject toJson() {
    JSONObject object = new JSONObject();
    object.put("first_name", this.first_name);
    object.put("last_name", this.last_name);
    object.put("email", this.email);

    if (!custom_fields.isEmpty()) {
      object.put("custom_fields", this.custom_fields);
    }

    return object;
  }
}