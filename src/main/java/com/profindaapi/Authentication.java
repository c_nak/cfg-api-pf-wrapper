package com.profindaapi;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;

import java.io.IOException;

public class Authentication {

  private String REST_URL;

  public Authentication() {
    this.REST_URL = "http://api.profinda.com/api/jml/authenticate";
  }

  public Authentication(String endpoint) { REST_URL = endpoint; }

  public JSONObject getCredentials(String email, String password) {
    JSONObject cred = new JSONObject();
    cred.put("password", password);
    cred.put("email", email);
    return cred;
  }

  public HttpResponse authenticate(String email, String password) {

    JSONObject user = new JSONObject();
    JSONObject cred = getCredentials(email, password);
    user.put("user", cred);

    String jsonData = user.toString();

    HttpReqCreator httpPostReq = new HttpReqCreator();
    HttpPost httpPost = (HttpPost) httpPostReq.createHttpRequest(new HttpPost(REST_URL), null);

    try {
      return httpPostReq.executeHttpRequest(jsonData, httpPost);
    } catch (IOException e) {
      e.printStackTrace();
    }
    finally {
//        httpPost.releaseConnection();
//      httpPost.getEntity().consumeContent();
//      try {
////        EntityUtils.consume(httpPost.getEntity());
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
    }
    return null;
  }
}
