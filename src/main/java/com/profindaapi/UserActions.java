package com.profindaapi;

import com.profindaapi.Exceptions.UserNotFoundException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Set;

public class UserActions {

    private HttpClient client;
    private HttpReqCreator httpReqCreator = new HttpReqCreator();

    public UserActions(HttpClient client) {
        this.client = client;
    }

    public UserActions() {
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        requestBuilder = requestBuilder.setConnectTimeout(2000);
        requestBuilder = requestBuilder.setConnectionRequestTimeout(2000);
        requestBuilder = requestBuilder.setSocketTimeout(15000);
        requestBuilder = requestBuilder.setExpectContinueEnabled(false);

        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setDefaultRequestConfig(requestBuilder.build());

        CloseableHttpClient client = builder.build();

//        this.client = builder.build();
        this.client = client;
    }

    public long getUserIdByEmail(String email, String authToken) throws UserNotFoundException {
        return getUserIdByEmail(null, email, authToken);
    }

    public Long getUserIdByEmail(String endpoint, String email, String authToken) throws UserNotFoundException {

        HttpResponse response = null;
        endpoint = getUsersEndpoint(endpoint, email);

        httpReqCreator = new HttpReqCreator();
        HttpGet httpGet = (HttpGet) httpReqCreator.createHttpRequest(new HttpGet(endpoint), authToken);

        try {
            response = client.execute(httpGet);

            Long jsonResponse = getIdFromJson(response);
            if (jsonResponse != null) return jsonResponse;
        } catch (ConnectionPoolTimeoutException e) {
            e.printStackTrace();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        throw new UserNotFoundException();
    }

    public HttpResponse updateUserById(long id, String authToken, JSONObject customFieldPayload) {
        return updateUserById(null, id, authToken, customFieldPayload);
    }

    public HttpResponse updateUserRubyCertificate(long id, String authToken) {
        JSONObject payload = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            payload = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                    "\"values\": [{\"id\": \"459390\",\"key\": \"ruby-for-web-development\",\"global_id\": null, \"curated_at\": null," +
                    " \"admin_approved\": true, \"text\": \"Ruby for web development\", \"experience\":null, \"interest\":null }]}]}");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updateUserById(id, authToken, payload);
    }

    public HttpResponse updateUserPythonCertificate(long id, String authToken) {
        JSONObject payload = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            payload = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                    "\"values\": [{\"id\": \"459390\",\"key\": \"python-for-web-development\",\"global_id\": null, \"curated_at\": null," +
                    " \"admin_approved\": true, \"text\": \"Python for web development\", \"experience\":null, \"interest\":null }]}]}");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updateUserById(id, authToken, payload);
    }

    public HttpResponse updateUserBeginnerCertificate(long id, String authToken) {
        JSONObject payload = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            payload = (JSONObject) parser.parse("{\"custom_fields\": [{\"type\": {\"id\": 1518,\"name\": \"cfg_certifications\"}," +
                    "\"values\": [{\"id\": \"459390\",\"key\": \"beginners-front-end-web-development-html-css\",\"global_id\": null, \"curated_at\": null," +
                    " \"admin_approved\": true, \"text\": \"Beginner's front end web development (HTML / CSS)\", \"experience\":null, \"interest\":null }]}]}");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updateUserById(id, authToken, payload);
    }

    public HttpResponse updateUserById(String endpoint, long id, String authToken, JSONObject customFieldPayload) {
        if (endpoint == null) {
            endpoint = "https://api.profinda.com/api/admin/profiles/" + id;
        }

        HttpReqCreator httpReqCreator = new HttpReqCreator();
        HttpPut httpPut = (HttpPut) httpReqCreator.createHttpRequest(new HttpPut(endpoint), authToken);

        try {
            return httpReqCreator.executeHttpRequest(customFieldPayload.toJSONString(), httpPut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse updateUserPermissionById(String endpoint, long userId, long permissionId, String authToken) {
        if (endpoint == null) {
            endpoint = "https://api.profinda.com/api/jml/users/" + userId;
        }

        JSONObject payload = new JSONObject();
        JSONParser parser = new JSONParser();

        try {
            payload = (JSONObject) parser.parse("{\"permissions_group_id\": " + permissionId + "}");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HttpReqCreator httpReqCreator = new HttpReqCreator();
        HttpPut httpPut = (HttpPut) httpReqCreator.createHttpRequest(new HttpPut(endpoint), authToken);

        try {
            return httpReqCreator.executeHttpRequest(payload.toJSONString(), httpPut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getUsersEndpoint(String endpoint, String email) {
        if (endpoint == null) {
            endpoint = "http://api.profinda.com/api/jml/users?per_page=1&search_by=" + email;
        }
        return endpoint;
    }

    private Long getIdFromJson(HttpResponse response) throws IOException, ParseException {
        if (response != null) {
            JSONArray jsonArray = (JSONArray) httpReqCreator.parseJsonResponse(response).get("entries");

            if (jsonArray != null && jsonArray.size() > 0) {
                JSONObject jsonResponse = (JSONObject) jsonArray.get(0);
                return (Long) jsonResponse.get("id");
            }
        }
        return null;
    }

    private void printOutResponse(JSONObject response) {
        Set<Object> set = response.keySet();

        for (Object obj : set) {
            System.out.println();
            System.out.println(obj);
        }
    }
}
