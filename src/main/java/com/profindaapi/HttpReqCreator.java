package com.profindaapi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HttpReqCreator {

    /**
     * @param request
     * @param authToken
     * @return
     */
    public HttpRequestBase createHttpRequest(HttpRequestBase request, String authToken) {

        request.setHeader("Content-Type", "application/json; charset=UTF-8");
        request.setHeader("Accept", "application/vnd.profinda+json;version=2");
        request.setHeader("Connection", "close, TE");

        if (System.getenv("PROFINDAACCOUNTDOMAIN") != null) {
            request.setHeader("PROFINDAACCOUNTDOMAIN", System.getenv("PROFINDAACCOUNTDOMAIN"));
        } else {
            try {
                throw new Exception("PROFINDAACCOUNTDOMAIN system env not set");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (authToken == null) {
            request.setHeader("profindaapitoken", "");
        } else {
            request.setHeader("profindaapitoken", authToken);
        }
        return request;
    }

    public HttpResponse executeHttpRequest(String jsonData, HttpEntityEnclosingRequestBase httpRequest) throws IOException {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        try {
            httpRequest.setEntity(new StringEntity(jsonData));

            return client.execute(httpRequest);

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
//            EntityUtils.consume(httpRequest.getEntity());
//            httpRequest.releaseConnection();
        }
        return null;
    }

    public JSONObject parseJsonResponse(HttpResponse response) throws IOException, ParseException {
        JSONObject jsonResponse;
        String line;
        StringBuilder result = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }
        JSONParser parser = new JSONParser();
        jsonResponse = (JSONObject) parser.parse(result.toString());

        return jsonResponse;
    }
}